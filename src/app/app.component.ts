import { Component } from '@angular/core';

interface UserModel {
  name: string;
  lastName: string;
  email: string;
  adress: string;
  gender: string;
  job: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  userModel: UserModel = {
    name: '',
    lastName: '',
    email: '',
    adress: '',
    gender: '',
    job: '',
  };

  users: UserModel[] = [];

  saveData() {
    this.users.push(this.userModel);
    console.log(this.userModel);
    console.log(this.users);
  }
}
